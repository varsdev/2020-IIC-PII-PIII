﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transito.BOL;
using Transito.ENTITIES;

namespace Transito.GUI
{
    public partial class FrmCiudadano : Form
    {
        public AccidenteBOL aBOL;
        public EPersona logeado;
        public VehiculoBOL vBOL;
        public FrmCiudadano(EPersona logeado)
        {
            InitializeComponent();
            aBOL = new AccidenteBOL();
            this.logeado = logeado;
            vBOL = new VehiculoBOL();
            LLenarData();
        }
        /// <summary>
        /// Borra la información de los texbox
        /// </summary>
        private void Limpiar()
        {
            cbxC.ResetText();
            cbxL.ResetText();
        }
        /// <summary>
        /// Llena el datagriview con los accidentes del usuario
        /// </summary>
        private void LLenarData()
        {
            foreach (DataGridViewRow row in dgvA.Rows)
            {
                dgvA.Rows.Clear();
            }
            List<EAccidente> acc = aBOL.PorCedula(logeado.cedula);

            foreach (EAccidente a in acc)
            {
                dgvA.Rows.Add(new object[] { a.codigo, a.ced_usu, a.lugar, a.placa, a.estado, a.fecha, a.multa });
            }
        }



        private void FrmCiudadano_Load(object sender, EventArgs e)
        {
            cbxC.DataSource = vBOL.ListaCarros(logeado.cedula);
            LLenarData();
        }

        private void BtnRe_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                EAccidente ac = new EAccidente();
                ac.ced_usu = logeado.cedula;
                ac.fecha = DateTime.Now;
                ac.num_parte = 0;
                ac.ced_juzg = "";
                ac.ced_ofic = "";
                ac.num_registro = 0;
                EVehiculo v = cbxC.SelectedItem as EVehiculo;
                ac.placa = v.placa;
                ac.estado = "Abierto";
                ac.multa = aBOL.Multa(v);
                ac.lugar = cbxL.SelectedItem.ToString();
                ac.codigo = aBOL.GenerarCod();
                aBOL.Registrar(ac);
                Limpiar();
                LLenarData();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                if (dgvA.SelectedRows.Count > 0)
                {
                    if (dgvA.CurrentRow.Cells[0].Value != null)
                    {
                        string codigo = dgvA.CurrentRow.Cells[0].Value.ToString();
                        Console.WriteLine(codigo);
                        aBOL.Eliminar(codigo);

                    }
                    LLenarData();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }


        }
    }
}
