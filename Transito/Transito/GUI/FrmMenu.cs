﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transito.ENTITIES;
using Transito.GUI;

namespace Transito
{
    public partial class FrmMenu : Form
    {
        public EPersona p;
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmMenu(EPersona p)
        {
            InitializeComponent();
            CenterToScreen();
            this.p = p;
        }
        /// <summary>
        /// Muestra los botones segun el tipo de usuario
        /// </summary>
        private void mostrar()
        {
            int tipo = p.tipo;
            switch (tipo)
            {
                case 1:
                    btnCiudadano.Visible = true;
                    btnJuzgado.Visible = false;
                    btnTransito.Visible = false;
                    btnReporteMulta.Visible = false;
                    btnReporteGeneral.Visible = false;
                    btnCiudadano.Location = new Point(0, 0);
                    break;

                case 2:
                    btnTransito.Visible = true; 
                    btnJuzgado.Visible = false;
                    btnReporteMulta.Visible = false;
                    btnReporteGeneral.Visible = false;
                    btnCiudadano.Visible = false;
                    btnTransito.Location = new Point(0, 0);
                    break;
                case 3:
                    btnJuzgado.Visible = true;
                    btnReporteMulta.Visible = true;
                    btnReporteGeneral.Visible = true;
                    btnTransito.Visible = false;
                    btnCiudadano.Visible = false;
                    btnJuzgado.Location = new Point(0, 0);
                    btnReporteGeneral.Location = new Point(0, 94);
                    break;
            }
        }
        /// <summary>
        /// Añade un form a un panel
        /// </summary>
        /// <param name="o">
        /// Objeto de tipo Form
        /// </param>
        private void AbrirForm(Object o)
        {
            if (this.panFrame.Controls.Count > 0)
            {
                this.panFrame.Controls.RemoveAt(0);
            }
            Form f = o as Form;
            f.TopLevel = false;
            f.Dock = DockStyle.Fill;
            this.panFrame.Controls.Add(f);
            this.panFrame.Tag = f;
            f.Show();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panFrame_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar sesion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {
            mostrar();
            lblNombre.Text = p.nombre;
        }

        private void BtnCiudadano_Click(object sender, EventArgs e)
        {
            AbrirForm(new FrmCiudadano(p));
        }

        private void btnTransito_Click(object sender, EventArgs e)
        {
            AbrirForm(new FrmTransito(p));
        }

        private void btnJuzgado_Click(object sender, EventArgs e)
        {
            AbrirForm(new FrmJuzgado(p));
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            AbrirForm(new FrmReporteMulta());
        }

        private void btnReporteGeneral_Click(object sender, EventArgs e)
        {
            AbrirForm(new FrmReporteGeneral());
        }
    }
}
