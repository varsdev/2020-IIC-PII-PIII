﻿namespace Transito
{
    partial class FrmMenu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnReporteGeneral = new System.Windows.Forms.Button();
            this.btnCiudadano = new System.Windows.Forms.Button();
            this.btnTransito = new System.Windows.Forms.Button();
            this.btnReporteMulta = new System.Windows.Forms.Button();
            this.btnJuzgado = new System.Windows.Forms.Button();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.panFrame = new System.Windows.Forms.Panel();
            this.lblNombre = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnMinimizar = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(38)))), ((int)(((byte)(43)))));
            this.panel1.Controls.Add(this.btnReporteGeneral);
            this.panel1.Controls.Add(this.btnCiudadano);
            this.panel1.Controls.Add(this.btnTransito);
            this.panel1.Controls.Add(this.btnReporteMulta);
            this.panel1.Controls.Add(this.btnJuzgado);
            this.panel1.Controls.Add(this.btnCerrarSesion);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 461);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // btnReporteGeneral
            // 
            this.btnReporteGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(38)))), ((int)(((byte)(43)))));
            this.btnReporteGeneral.FlatAppearance.BorderSize = 0;
            this.btnReporteGeneral.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnReporteGeneral.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(71)))));
            this.btnReporteGeneral.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReporteGeneral.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporteGeneral.ForeColor = System.Drawing.Color.White;
            this.btnReporteGeneral.Image = ((System.Drawing.Image)(resources.GetObject("btnReporteGeneral.Image")));
            this.btnReporteGeneral.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReporteGeneral.Location = new System.Drawing.Point(0, 188);
            this.btnReporteGeneral.Name = "btnReporteGeneral";
            this.btnReporteGeneral.Size = new System.Drawing.Size(200, 41);
            this.btnReporteGeneral.TabIndex = 13;
            this.btnReporteGeneral.Text = "   Reporte accidentes";
            this.btnReporteGeneral.UseVisualStyleBackColor = false;
            this.btnReporteGeneral.Click += new System.EventHandler(this.btnReporteGeneral_Click);
            // 
            // btnCiudadano
            // 
            this.btnCiudadano.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(38)))), ((int)(((byte)(43)))));
            this.btnCiudadano.FlatAppearance.BorderSize = 0;
            this.btnCiudadano.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCiudadano.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(71)))));
            this.btnCiudadano.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCiudadano.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCiudadano.ForeColor = System.Drawing.Color.White;
            this.btnCiudadano.Image = ((System.Drawing.Image)(resources.GetObject("btnCiudadano.Image")));
            this.btnCiudadano.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCiudadano.Location = new System.Drawing.Point(0, 141);
            this.btnCiudadano.Name = "btnCiudadano";
            this.btnCiudadano.Size = new System.Drawing.Size(200, 41);
            this.btnCiudadano.TabIndex = 12;
            this.btnCiudadano.Text = "Ciudadano";
            this.btnCiudadano.UseVisualStyleBackColor = false;
            this.btnCiudadano.Click += new System.EventHandler(this.BtnCiudadano_Click);
            // 
            // btnTransito
            // 
            this.btnTransito.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(38)))), ((int)(((byte)(43)))));
            this.btnTransito.FlatAppearance.BorderSize = 0;
            this.btnTransito.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTransito.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(71)))));
            this.btnTransito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransito.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransito.ForeColor = System.Drawing.Color.White;
            this.btnTransito.Image = ((System.Drawing.Image)(resources.GetObject("btnTransito.Image")));
            this.btnTransito.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTransito.Location = new System.Drawing.Point(0, 94);
            this.btnTransito.Name = "btnTransito";
            this.btnTransito.Size = new System.Drawing.Size(200, 41);
            this.btnTransito.TabIndex = 11;
            this.btnTransito.Text = "Transito";
            this.btnTransito.UseMnemonic = false;
            this.btnTransito.UseVisualStyleBackColor = false;
            this.btnTransito.Click += new System.EventHandler(this.btnTransito_Click);
            // 
            // btnReporteMulta
            // 
            this.btnReporteMulta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(38)))), ((int)(((byte)(43)))));
            this.btnReporteMulta.FlatAppearance.BorderSize = 0;
            this.btnReporteMulta.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnReporteMulta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(71)))));
            this.btnReporteMulta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReporteMulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporteMulta.ForeColor = System.Drawing.Color.White;
            this.btnReporteMulta.Image = ((System.Drawing.Image)(resources.GetObject("btnReporteMulta.Image")));
            this.btnReporteMulta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReporteMulta.Location = new System.Drawing.Point(0, 47);
            this.btnReporteMulta.Name = "btnReporteMulta";
            this.btnReporteMulta.Size = new System.Drawing.Size(200, 41);
            this.btnReporteMulta.TabIndex = 10;
            this.btnReporteMulta.Text = "   Multas por aprobar";
            this.btnReporteMulta.UseVisualStyleBackColor = false;
            this.btnReporteMulta.Click += new System.EventHandler(this.btnReportes_Click);
            // 
            // btnJuzgado
            // 
            this.btnJuzgado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(38)))), ((int)(((byte)(43)))));
            this.btnJuzgado.FlatAppearance.BorderSize = 0;
            this.btnJuzgado.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnJuzgado.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(71)))));
            this.btnJuzgado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnJuzgado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJuzgado.ForeColor = System.Drawing.Color.White;
            this.btnJuzgado.Image = global::Transito.Properties.Resources.icons8_courthouse_32px;
            this.btnJuzgado.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnJuzgado.Location = new System.Drawing.Point(0, 0);
            this.btnJuzgado.Name = "btnJuzgado";
            this.btnJuzgado.Size = new System.Drawing.Size(200, 41);
            this.btnJuzgado.TabIndex = 9;
            this.btnJuzgado.Text = "Juzgado";
            this.btnJuzgado.UseVisualStyleBackColor = false;
            this.btnJuzgado.Click += new System.EventHandler(this.btnJuzgado_Click);
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(38)))), ((int)(((byte)(43)))));
            this.btnCerrarSesion.FlatAppearance.BorderSize = 0;
            this.btnCerrarSesion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCerrarSesion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(71)))));
            this.btnCerrarSesion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarSesion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrarSesion.ForeColor = System.Drawing.Color.White;
            this.btnCerrarSesion.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrarSesion.Image")));
            this.btnCerrarSesion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrarSesion.Location = new System.Drawing.Point(0, 409);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(200, 41);
            this.btnCerrarSesion.TabIndex = 8;
            this.btnCerrarSesion.Text = "Cerrar Sesion";
            this.btnCerrarSesion.UseVisualStyleBackColor = false;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // panFrame
            // 
            this.panFrame.Location = new System.Drawing.Point(216, 41);
            this.panFrame.Name = "panFrame";
            this.panFrame.Size = new System.Drawing.Size(571, 409);
            this.panFrame.TabIndex = 13;
            this.panFrame.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panFrame_MouseDown);
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.Black;
            this.lblNombre.Location = new System.Drawing.Point(255, 12);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(65, 20);
            this.lblNombre.TabIndex = 15;
            this.lblNombre.Text = "Nombre";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(216, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(33, 35);
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.Image")));
            this.btnMinimizar.Location = new System.Drawing.Point(743, 12);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(15, 15);
            this.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimizar.TabIndex = 12;
            this.btnMinimizar.TabStop = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.Location = new System.Drawing.Point(773, 12);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(15, 15);
            this.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnCerrar.TabIndex = 11;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(181)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(800, 461);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panFrame);
            this.Controls.Add(this.btnMinimizar);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmMenu";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox btnMinimizar;
        private System.Windows.Forms.PictureBox btnCerrar;
        private System.Windows.Forms.Panel panFrame;
        private System.Windows.Forms.Button btnCiudadano;
        private System.Windows.Forms.Button btnTransito;
        private System.Windows.Forms.Button btnReporteMulta;
        private System.Windows.Forms.Button btnJuzgado;
        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Button btnReporteGeneral;
    }
}

