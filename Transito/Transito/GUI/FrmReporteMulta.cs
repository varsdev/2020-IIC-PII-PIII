﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transito.BOL;
using Transito.ENTITIES;

namespace Transito.GUI
{
    public partial class FrmReporteMulta : Form
    {
        public AccidenteBOL aBOL;

        public FrmReporteMulta()
        {
            InitializeComponent();
            aBOL = new AccidenteBOL();
            LLenarData();
        }

        private void LLenarData()
        {
            foreach (DataGridViewRow row in dgvA.Rows)
            {
                dgvA.Rows.Clear();
            }
            List<EAccidente> acc = aBOL.PorEstado("Por aprobar");


            foreach (EAccidente a in acc)
            {
                if (a.multa < 450000)
                {
                    dgvA.Rows.Add(new object[] { a.codigo, a.ced_ofic, a.multa });
                }               
            }
        }
    }
}
