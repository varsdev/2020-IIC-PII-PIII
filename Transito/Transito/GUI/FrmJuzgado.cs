﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transito.BOL;
using Transito.ENTITIES;

namespace Transito.GUI
{
    public partial class FrmJuzgado : Form
    {
        public AccidenteBOL aBOL;
        public EPersona logeado;
        public EAccidente accidente;

        public FrmJuzgado(EPersona logeado)
        {
            InitializeComponent();
            this.logeado = logeado;
            aBOL = new AccidenteBOL();
            LLenarData();
        }

        private void LLenarData()
        {
            foreach (DataGridViewRow row in dgvA.Rows)
            {
                dgvA.Rows.Clear();
            }
            List<EAccidente> acc = aBOL.PorEstado("Por aprobar");

            foreach (EAccidente a in acc)
            {
                dgvA.Rows.Add(new object[] { a, a.codigo, a.ced_usu, a.lugar, a.placa, a.ced_ofic, a.num_parte,
                    a.ced_juzg, a.num_registro, a.estado, a.fecha, a.multa });
            }
        }

        private void btnRe_Click(object sender, EventArgs e)
        {
            try
            {
                if (accidente != null)
                {
                    accidente.ced_juzg = logeado.cedula;
                    accidente.num_registro = int.Parse(txtNumeroRegistro.Text.Trim());
                    accidente.estado = "Completo";
                    accidente.fecha = DateTime.Now;
                    Console.WriteLine(accidente);
                    aBOL.EditarJuzgado(accidente);
                    LLenarData();
                    txtNumeroRegistro.ResetText();
                    lblError.Text = "Numero registrado";
                }
            }
            catch (Exception)
            {
                lblError.Text = "Intente nuevamente";
            }
        }

        private void txtNumeroRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void dgvA_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                btnRe.Enabled = true;
                accidente = (EAccidente)dgvA.Rows[e.RowIndex].Cells[0].Value;
            }
            catch (Exception)
            {

            }
        }
    }
}
