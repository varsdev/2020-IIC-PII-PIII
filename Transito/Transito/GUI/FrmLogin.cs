﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transito.BOL;
using Transito.DAL;
using Transito.ENTITIES;

namespace Transito.GUI
{
    public partial class FrmLogin : Form
    {
        public XmlBOL xBOL;
        public PersonaBOL pBOL;
        public PersonaDAL pdal;
        public VehiculoDAL vdal;

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmLogin()
        {
            InitializeComponent();
            CenterToScreen();
            xBOL = new XmlBOL();
            pBOL = new PersonaBOL();
            pdal = new PersonaDAL();
            vdal = new VehiculoDAL();

        }

        private void txtCedula_Enter(object sender, EventArgs e)
        {
            if (txtCedula.Text == "Cedula")
            {
                txtCedula.Text = "";
                txtCedula.ForeColor = Color.LightGray;
                lblMensaje.Visible = false;

            }
        }

        private void txtContra_Enter(object sender, EventArgs e)
        {
            if (txtContra.Text == "Contraseña")
            {
                txtContra.Text = "";
                txtContra.ForeColor = Color.LightGray;
                txtContra.UseSystemPasswordChar = true;
                lblMensaje.Visible = false;
            }
        }

        private void txtCedula_Leave(object sender, EventArgs e)
        {
            if (txtCedula.Text == "")
            {
                txtCedula.Text = "Cedula";
                txtCedula.ForeColor = Color.DarkGray;
            }
        }

        private void txtContra_Leave(object sender, EventArgs e)
        {
            if (txtContra.Text == "")
            {
                txtContra.Text = "Contraseña";
                txtContra.ForeColor = Color.DarkGray;
                txtContra.UseSystemPasswordChar = false;
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        private void txtCedula_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void txtContra_MouseClick(object sender, MouseEventArgs e)
        {
            lblMensaje.Visible = false;
        }

        private void pcbVerContra_MouseDown(object sender, MouseEventArgs e)
        {
            txtContra.UseSystemPasswordChar = false;
            pcbVerContra.Image = Transito.Properties.Resources.icons8_invisible_32px;
        }

        private void pcbVerContra_MouseUp(object sender, MouseEventArgs e)
        {
            txtContra.UseSystemPasswordChar = true;
            pcbVerContra.Image = Transito.Properties.Resources.icons8_eye_32px;
        }

        private void Logout(object sender, FormClosedEventArgs e)
        {
            txtCedula.Text = "Cedula";
            txtContra.Text = "Contraseña";
            lblMensaje.Visible = false;
            this.Show();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtCedula.Text != "Cedula")
            {
                if (txtContra.Text != "Contraseña")
                {
                    try
                    {
                        string cedula = txtCedula.Text.Trim();
                        string contra = txtContra.Text.Trim();
                        EPersona p = pBOL.Logearse(cedula, contra);
                        TiposMenu(p);
                    }
                    catch (Exception ex)
                    {
                        msgError(ex.Message);
                    }
                }
                else
                {
                    msgError("Por favor digite su contraseña");
                }
            }
            else
            {
                msgError("Por favor digite su cedula");
            }
        }

        private void TiposMenu(EPersona p)
        {
            int tipo = p.tipo;
            switch (tipo)
            {
                case 1:
                    this.Hide();
                    FrmMenu frm = new FrmMenu(p);
                    frm.Show();
                    frm.FormClosed += Logout;
                    break;
                case 2:
                    this.Hide();
                    frm = new FrmMenu(p);
                    frm.FormClosed += Logout;
                    frm.Show();
                    break;
                case 3:
                    this.Hide();
                    frm = new FrmMenu(p);
                    frm.FormClosed += Logout;
                    frm.Show();
                    break;
            }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            xBOL.CrearArchivo();
        }

    }
}
