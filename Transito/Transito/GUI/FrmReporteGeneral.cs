﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transito.BOL;
using Transito.ENTITIES;

namespace Transito.GUI
{
    public partial class FrmReporteGeneral : Form
    {
        public AccidenteBOL aBOL;
        public VehiculoBOL vBOL;
        public EVehiculo v;

        public FrmReporteGeneral()
        {
            InitializeComponent();
            aBOL = new AccidenteBOL();
            vBOL = new VehiculoBOL();
            v = new EVehiculo();
            LLenarData();
        }

        private void LLenarData()
        {
            foreach (DataGridViewRow row in dgvA.Rows)
            {
                dgvA.Rows.Clear();
            }
            List<EAccidente> acc = aBOL.PorEstado("Completo");
            

            foreach (EAccidente a in acc)
            {
                v = vBOL.CargarVehiculo(a.placa);
                dgvA.Rows.Add(new object[] { a.ced_usu, a.num_parte, a.lugar, a.estado, a.fecha, v.anno,
                v.marca, v.color, TipoVehiculo(v.tipo)});
            }
        }

        public string TipoVehiculo(int tipo)
        {
            switch (tipo)
            {
                case 1:
                    return "Automovil";
                case 2:
                    return "Motocicleta";
                case 3:
                    return "Camion";
            }
            return "";
        }

    }
}
