﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Transito.ENTITIES;

namespace Transito.DAL
{
   public class AccidenteDAL
    {
        /// <summary>
        /// Crea el nodo para el tipo de objeto Accidente
        /// </summary>
        /// <param name="a">
        /// objeto Accidente
        /// </param>
        /// <returns>
        /// objeto de tipo XmlNode
        /// </returns>
        public XmlNode Crear(EAccidente a)
        {
            XmlNode acc = Conexion.doc.CreateElement("accidente");

            XmlElement xcod = Conexion.doc.CreateElement("codigo");
            xcod.InnerText = a.codigo;
            acc.AppendChild(xcod);

            XmlElement xcedus = Conexion.doc.CreateElement("ced_usu");
            xcedus.InnerText = a.ced_usu;
            acc.AppendChild(xcedus);

            XmlElement xlug = Conexion.doc.CreateElement("lugar");
            xlug.InnerText = a.lugar;
            acc.AppendChild(xlug);

            XmlElement xplac = Conexion.doc.CreateElement("placa");
            xplac.InnerText = a.placa;
            acc.AppendChild(xplac);

            XmlElement xcedof = Conexion.doc.CreateElement("ced_ofi");
            xcedof.InnerText = a.ced_ofic;
            acc.AppendChild(xcedof);

            XmlElement xnp = Conexion.doc.CreateElement("nparte");
            xnp.InnerText = a.num_parte.ToString();
            acc.AppendChild(xnp);


            XmlElement xcedj = Conexion.doc.CreateElement("ced_juz");
            xcedj.InnerText = a.ced_juzg;
            acc.AppendChild(xcedj);

            XmlElement xre = Conexion.doc.CreateElement("nregistro");
            xre.InnerText = a.num_registro.ToString();
            acc.AppendChild(xre);


            XmlElement xest = Conexion.doc.CreateElement("estado");
            xest.InnerText = a.estado;
            acc.AppendChild(xest);

            XmlElement xfech = Conexion.doc.CreateElement("fecha");
            xfech.InnerText = a.fecha.ToString();
            acc.AppendChild(xfech);

            XmlElement xmul = Conexion.doc.CreateElement("multa");
            xmul.InnerText = a.multa.ToString();
            acc.AppendChild(xmul);


            return acc;
        }

        /// <summary>
        /// Valida si un vehiculo ya tiene un accidente registrado
        /// </summary>
        /// <param name="placa">
        /// Placa de vehiculo
        /// </param>
        /// <returns>
        /// False si ya tiene un accidente registrado y true si no
        /// </returns>
        public bool ValidarV(string placa)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList acc = Conexion.doc.SelectNodes("Transito/accidente");


            foreach (XmlNode x in acc)
            {
                if (x.SelectSingleNode("placa").InnerText == placa)
                     
                {                   
                    return false;
                }


            }
            return true;
        }

        /// <summary>
        /// Elimina un accidente por codigo y estado
        /// </summary>
        /// <param name="cod">
        /// Codigo del accidente
        /// </param>
        public void Eliminar(string cod)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList lista = Conexion.doc.SelectNodes("Transito/accidente");

            XmlNode borra = Conexion.doc.DocumentElement;

            foreach (XmlNode item in lista)
            {
                if (item.SelectSingleNode("codigo").InnerText == cod &&
                    item.SelectSingleNode("estado").InnerText == "Abierto" )
                {
                    XmlNode borrar = item;
                    borra.RemoveChild(borrar);
                    
                }

            }
            Conexion.doc.Save(Conexion.rutaXml);
        }

        /// <summary>
        /// Busca los accidentes por medio de la cedula del usuario
        /// </summary>
        /// <param name="ced">
        /// Cedula del usuario
        /// </param>
        /// <returns>
        /// Lista de accidentes
        /// </returns>
        public List<EAccidente> AccidentesPorCed(string ced)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList lista = Conexion.doc.SelectNodes("Transito/accidente");
            List<EAccidente> accidentes = new List<EAccidente>();

            foreach (XmlNode item in lista)
            {
                if (item.SelectSingleNode("ced_usu").InnerText == ced)
                {
                    EAccidente a = new EAccidente();
                    a.codigo= item.SelectSingleNode("codigo").InnerText;
                    a.ced_usu= item.SelectSingleNode("ced_usu").InnerText;
                    a.placa = item.SelectSingleNode("placa").InnerText;
                    a.estado= item.SelectSingleNode("estado").InnerText;
                    a.fecha= DateTime.Parse(item.SelectSingleNode("fecha").InnerText);
                    a.multa = Double.Parse(item.SelectSingleNode("multa").InnerText);
                    a.lugar= item.SelectSingleNode("lugar").InnerText;

                    accidentes.Add(a);
                }

            }
            return accidentes;
        }

        /// <summary>
        /// Busca los accidentes por medio del estado 
        /// </summary>
        /// <param name="estado">
        /// Estado del accidente
        /// </param>
        /// <returns>
        /// Lista de accidentes
        /// </returns>
        public List<EAccidente> AccidentesPorEstado(string estado)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList lista = Conexion.doc.SelectNodes("Transito/accidente");
            List<EAccidente> accidentes = new List<EAccidente>();

            foreach (XmlNode item in lista)
            {
                if (item.SelectSingleNode("estado").InnerText == estado)
                {
                    EAccidente a = new EAccidente();
                    a.codigo = item.SelectSingleNode("codigo").InnerText;
                    a.ced_usu = item.SelectSingleNode("ced_usu").InnerText;
                    a.placa = item.SelectSingleNode("placa").InnerText;
                    a.ced_ofic = item.SelectSingleNode("ced_ofi").InnerText;
                    a.num_parte = int.Parse(item.SelectSingleNode("nparte").InnerText);
                    a.estado = item.SelectSingleNode("estado").InnerText;
                    a.fecha = DateTime.Parse(item.SelectSingleNode("fecha").InnerText);
                    a.multa = Double.Parse(item.SelectSingleNode("multa").InnerText);
                    a.lugar = item.SelectSingleNode("lugar").InnerText;

                    accidentes.Add(a);
                }

            }
            return accidentes;
        }

        /// <summary>
        /// Guarda la información de una accidente en el archivo
        /// </summary>
        /// <param name="ac">
        /// Accidente a guardar
        /// </param>
        public void AgregarDatos(EAccidente ac)
        {
            if (ValidarV(ac.placa))
            {
                Conexion.doc.Load(Conexion.rutaXml);

                XmlNode accidente = Crear(ac);

                XmlNode nodo = Conexion.doc.DocumentElement;

                nodo.InsertAfter(accidente, nodo.LastChild);

                Conexion.doc.Save(Conexion.rutaXml);

            }
            else
            {
                throw new Exception("Información incorrecta");
            }


        }

        /// <summary>
        /// Edita los nodos que son referentes al transito
        /// </summary>
        /// <param name="ac"></param>
        /// Accidente que se editara
        public void EditarTransito(EAccidente ac)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList lista = Conexion.doc.SelectNodes("Transito/accidente");

            foreach (XmlNode item in lista)
            {
                if (item.SelectSingleNode("codigo").InnerText == ac.codigo)
                {
                    item.SelectSingleNode("ced_ofi").InnerText = ac.ced_ofic;
                    item.SelectSingleNode("nparte").InnerText = ac.num_parte.ToString();
                    item.SelectSingleNode("fecha").InnerText = ac.fecha.ToString();
                    item.SelectSingleNode("estado").InnerText = ac.estado;

                }
            }
            Conexion.doc.Save(Conexion.rutaXml);
        }

        /// <summary>
        /// Edita los nodos que son referentes al juzgado
        /// </summary>
        /// <param name="ac"></param>
        /// Accidente que se editara
        public void EditarJuzgado(EAccidente ac)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList lista = Conexion.doc.SelectNodes("Transito/accidente");

            foreach (XmlNode item in lista)
            {
                if (item.SelectSingleNode("codigo").InnerText == ac.codigo)
                {
                    item.SelectSingleNode("ced_juz").InnerText = ac.ced_juzg;
                    item.SelectSingleNode("nregistro").InnerText = ac.num_registro.ToString();
                    item.SelectSingleNode("fecha").InnerText = ac.fecha.ToString();
                    item.SelectSingleNode("estado").InnerText = ac.estado;

                }
            }
            Conexion.doc.Save(Conexion.rutaXml);
        }

    }
}
