﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Transito.ENTITIES;

namespace Transito.DAL
{
   public class VehiculoDAL
    {
        /// <summary>
        /// Crea el nodo para el objeto de tipo Vehiculo
        /// </summary>
        /// <param name="v">
        /// Objeto de tipo Vehiculo
        /// </param>
        /// <returns>
        /// objeto de tipo XmlNode
        /// </returns>
        public XmlNode Crear(EVehiculo v)
        {
            XmlNode veh = Conexion.doc.CreateElement("vehiculo");

            XmlElement xpla = Conexion.doc.CreateElement("placa");
            xpla.InnerText = v.placa;
            veh.AppendChild(xpla);

            XmlElement xanno = Conexion.doc.CreateElement("anno");
            xanno.InnerText = v.anno.ToString();
            veh.AppendChild(xanno);

            XmlElement xmar = Conexion.doc.CreateElement("marca");
            xmar.InnerText = v.marca;
            veh.AppendChild(xmar);

            XmlElement xcol = Conexion.doc.CreateElement("color");
            xcol.InnerText = v.color;
            veh.AppendChild(xcol);

            XmlElement xtipo = Conexion.doc.CreateElement("tipo");
            xtipo.InnerText = v.tipo.ToString();
            veh.AppendChild(xtipo);

            XmlElement xpro = Conexion.doc.CreateElement("propetario");
            xpro.InnerText = v.cedPropietario;
            veh.AppendChild(xpro);

            return veh;
        }

        /// <summary>
        /// Busca los vehiculos vinculados con un usuario
        /// </summary>
        /// <param name="ced">
        /// cedula del cliente
        /// </param>
        /// <returns>
        /// Lista de los vehiculos
        /// </returns>
        public List<Object> ListaPlacas(string ced)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList lista = Conexion.doc.SelectNodes("Transito/vehiculo");
            List<Object> placas = new List<object>();

            foreach (XmlNode item in lista)
            {
                if (item.SelectSingleNode("propetario").InnerText == ced)
                {
                    EVehiculo v = new EVehiculo();
                    v.placa = item.SelectSingleNode("placa").InnerText;
                    v.tipo= int.Parse(item.SelectSingleNode("tipo").InnerText);
                    v.anno= int.Parse(item.SelectSingleNode("anno").InnerText);
                    placas.Add(v);
                }
               
            }
            return placas;
        }

        /// <summary>
        /// Guarda la información de una vehiculo en el archivo
        /// </summary>
        /// <param name="v">
        ///  Vehiculo a guardar
        /// </param>
        public void AgregarDatos(EVehiculo v)
        {

            Conexion.doc.Load(Conexion.rutaXml);

            XmlNode veh = Crear(v);

            XmlNode nodo = Conexion.doc.DocumentElement;

            nodo.InsertAfter(veh, nodo.LastChild);

            Conexion.doc.Save(Conexion.rutaXml);

        }
    
        /// <summary>
        /// Busca la información de un vehiculo por medio de la placa
        /// </summary>
        /// <param name="placa">
        /// Placa del vehiculo
        /// </param>
        /// <returns>
        /// Objeto de tipo Vehiculo
        /// </returns>
        public EVehiculo CargarVehiculo(string placa)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList lista = Conexion.doc.SelectNodes("Transito/vehiculo");
            List<Object> placas = new List<object>(); 

            foreach (XmlNode item in lista)
            {
                if (item.SelectSingleNode("placa").InnerText == placa)
                {
                    EVehiculo v = new EVehiculo();
                    v.placa = item.SelectSingleNode("placa").InnerText;
                    v.tipo = int.Parse(item.SelectSingleNode("tipo").InnerText);
                    v.anno = int.Parse(item.SelectSingleNode("anno").InnerText);
                    v.color = item.SelectSingleNode("color").InnerText;
                    v.marca = item.SelectSingleNode("marca").InnerText;
                    return v;
                }

            }
            return null;
        }

    }
}
