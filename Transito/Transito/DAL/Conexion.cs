﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Transito.DAL
{
   public static class Conexion
    {
        public static XmlDocument doc;
        public static string rutaXml;

       static Conexion()
        {
            doc = new XmlDocument();
            rutaXml = "Datos.xml";
        }

        /// <summary>
        /// Crea el achivo xml
        /// </summary>
        /// <param name="nodoRaiz">
        /// Nombre del nodo principal
        /// </param>
        public static void CrearXML(string nodoRaiz)
        {
          
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");

            XmlNode nodo = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, nodo);

            XmlNode elemento = doc.CreateElement(nodoRaiz);
            doc.AppendChild(elemento);

            doc.Save(rutaXml);
        }
    }
}
