﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Transito.ENTITIES;

namespace Transito.DAL
{
    public class PersonaDAL
    {
        /// <summary>
        /// Crea los nodos para el tipo de objeto persona
        /// </summary>
        /// <param name="p">
        /// objeto Persona
        /// </param>
        /// <returns>
        /// objeto de tipo XmlNode
        /// </returns>
        public XmlNode Crear(EPersona p)
        {
            XmlNode pers = Conexion.doc.CreateElement("persona");

            XmlElement xced = Conexion.doc.CreateElement("cedula");
            xced.InnerText = p.cedula;
            pers.AppendChild(xced);

            XmlElement xnom = Conexion.doc.CreateElement("nombre");
            xnom.InnerText = p.nombre.ToString();
            pers.AppendChild(xnom);

            XmlElement xcon = Conexion.doc.CreateElement("contrasenna");
            xcon.InnerText = p.contrasenna;
            pers.AppendChild(xcon);

            XmlElement xtipo = Conexion.doc.CreateElement("tipo");
            xtipo.InnerText = p.tipo.ToString();
            pers.AppendChild(xtipo);

            return pers;
        }

        /// <summary>
        /// Guarda la información de una persona en el archivo
        /// </summary>
        /// <param name="p">
        /// Persona a guardar
        /// </param>
        public void AgregarDatos(EPersona p)
        {
          
            Conexion.doc.Load(Conexion.rutaXml);
 
            XmlNode platillo = Crear(p);

            XmlNode nodo = Conexion.doc.DocumentElement;

            nodo.InsertAfter(platillo, nodo.LastChild);

            Conexion.doc.Save(Conexion.rutaXml);
       
        }

        /// <summary>
        /// Busca si los datos requeridos para logearse son correctos
        /// </summary>
        /// <param name="cedula">
        /// Cedula del usuario
        /// </param>
        /// <param name="contrasenna">
        /// Contraseña del usuario
        /// </param>
        /// <returns>
        /// Un objeto de tipo Persona
        /// </returns>
        public EPersona Login(string cedula, string contrasenna)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList personas = Conexion.doc.SelectNodes("Transito/persona");

            foreach (XmlNode x in personas)
            {
                if (x.SelectSingleNode("cedula").InnerText == cedula &&
                        x.SelectSingleNode("contrasenna").InnerText == contrasenna)
                {
                    EPersona p = new EPersona();
                    p.nombre = x.SelectSingleNode("nombre").InnerText;
                    p.tipo = Int32.Parse(x.SelectSingleNode("tipo").InnerText);
                    p.cedula = x.SelectSingleNode("cedula").InnerText;
                    return p;
                }
            }
            throw new Exception("Información incorrecta");
        }

        /// <summary>
        /// Elimina una persoona 
        /// </summary>
        /// <param name="ced"></param>
        /// Cedula de la persona a eliminar
        public void Eliminar(string ced)
        {
            Conexion.doc.Load(Conexion.rutaXml);

            XmlNodeList lista = Conexion.doc.SelectNodes("Transito/persona");

            XmlNode borra = Conexion.doc.DocumentElement;

            foreach (XmlNode item in lista)
            {
                if (item.SelectSingleNode("cedula").InnerText == ced)
                {
                    XmlNode borrar = item;
                    borra.RemoveChild(borrar);

                    //item.ParentNode.RemoveChild(item);

                    //return true;
                }

            }
            Conexion.doc.Save(Conexion.rutaXml);



        }

    }
}
