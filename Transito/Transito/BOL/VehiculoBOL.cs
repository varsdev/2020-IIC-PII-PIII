﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transito.DAL;
using Transito.ENTITIES;

namespace Transito.BOL
{
    public class VehiculoBOL
    {
       
        /// <summary>
        /// Retorna una lista de los carros de un usuario por medio de la cedula
        /// </summary>
        /// <param name="ced">
        /// cedula del usuario
        /// </param>
        /// <returns>
        /// lista de los carros
        /// </returns>
        public List<Object> ListaCarros(string ced)
        {
            return new VehiculoDAL().ListaPlacas(ced);
        }

        /// <summary>
        /// Carga la información de un vehiculo por medio de la placa
        /// </summary>
        /// <param name="placa">
        /// Placa del vehiculo
        /// </param>
        /// <returns>
        /// Objeto de tipo Vehiculo
        /// </returns>
        public EVehiculo CargarVehiculo(string placa)
        {
            return new VehiculoDAL().CargarVehiculo(placa);
        }


    }
}
