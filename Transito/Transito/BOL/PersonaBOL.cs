﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transito.DAL;
using Transito.ENTITIES;

namespace Transito.BOL
{
    public class PersonaBOL
    {
        /// <summary>
        /// Valida los datos del login 
        /// </summary>
        /// <param name="ced">
        /// Cedula del usuario
        /// </param>
        /// <param name="contr">
        /// Contraseña del usuario
        /// </param>
        /// <returns>
        /// Objeto de tipo Persona
        /// </returns>
        public EPersona Logearse(string ced, string contr)
        {
            return new PersonaDAL().Login(ced, contr);
        }
    }
}
