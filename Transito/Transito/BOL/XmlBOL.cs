﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transito.DAL;

namespace Transito.BOL
{
    public class XmlBOL
    {
        /// <summary>
        /// Crea el archivo xml si no existe
        /// </summary>
        public void CrearArchivo()
        {
            if (!File.Exists(Conexion.rutaXml))
            {
                Conexion.CrearXML("Transito");

            }
           
        }
    }
}
