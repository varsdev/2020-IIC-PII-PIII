﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transito.DAL;
using Transito.ENTITIES;

namespace Transito.BOL
{
    public class AccidenteBOL
    {
        /// <summary>
        /// Guarda uno objeto de tipo Accidente en un archivo xml
        /// </summary>
        /// <param name="ac">
        /// Objeto a registrar
        /// </param>
        public void Registrar(EAccidente ac)
        {
           AccidenteDAL aDAL = new AccidenteDAL();
           aDAL.AgregarDatos(ac);                             
        }
       
        /// <summary>
        /// Edita info de transito en el nodo
        /// </summary>
        /// <param name="ac"></param>
        /// Objeto a editar
        public void EditarTransito(EAccidente ac)
        {
            AccidenteDAL aDAL = new AccidenteDAL();
            aDAL.EditarTransito(ac);
        }

        /// <summary>
        /// Edita info de juzgado en el nodo
        /// </summary>
        /// <param name="ac"></param>
        /// objeto a editar
        public void EditarJuzgado(EAccidente ac)
        {
            AccidenteDAL aDAL = new AccidenteDAL();
            aDAL.EditarJuzgado(ac);
        }

        /// <summary>
        /// Busca los accidentes de un usuario
        /// </summary>
        /// <param name="cedula">
        /// Cedula del usuario
        /// </param>
        /// <returns>
        /// Lista de tipo Accidente
        /// </returns>
        public List<EAccidente> PorCedula(string cedula)
        {
            return new AccidenteDAL().AccidentesPorCed(cedula);
        }

        /// <summary>
        /// Busca los accidentes de un usuario
        /// </summary>
        /// <param name="estado">
        /// Estado del accidente
        /// </param>
        /// <returns>
        /// Lista de tipo Accidente
        /// </returns>
        public List<EAccidente> PorEstado(string estado)
        {
            return new AccidenteDAL().AccidentesPorEstado(estado);
        }

        /// <summary>
        /// Calcula el monto de la multa segun el tipo de vehiculo
        /// </summary>
        /// <param name="v">
        /// Vehiculo del usuario
        /// </param>
        /// <returns>
        /// Monto de la multa
        /// </returns>
        public double Multa(EVehiculo v)
        {        
            int tipo = v.tipo;
            double multa = 0;
            double total = 0;
            switch (tipo)
            {
                case 1://carro
                    multa = (25000 * 0.30) + 25000;
                    break;
                case 2://moto
                    multa = (10000 * 0.15) + 10000;
                    break;
                case 3://camion
                    multa = (65000 * 0.70) + 65000;
                    break;
                case 4://bus
                    multa = (45000 * 0.45) + 45000;
                    break;                  
            }
            total= TotalMulta(multa, v.anno);
            
            return total;
        }

        /// <summary>
        /// Calcula el monto extra de la multa segun el año del vehiculo
        /// </summary>
        /// <param name="montoparcial">
        /// El monto parcial de la multa
        /// </param>
        /// <param name="anno">
        /// Año del vehiculo
        /// </param>
        /// <returns>
        /// Monto total
        /// </returns>
        private double TotalMulta(double montoparcial,int anno)
        {
            double multa = 0;
            if (anno < 2000)
            {
                multa = (montoparcial * 0.10) + montoparcial;
            }
            else
            {
                multa = montoparcial;
            }
            return multa;
        }

        /// <summary>
        /// Elimina un accidente por codigo
        /// </summary>
        /// <param name="cod">
        /// codigo del accidente
        /// </param>
        public void Eliminar(string cod)
        {
            AccidenteDAL aDAL = new AccidenteDAL();
            aDAL.Eliminar(cod);
        }

        /// <summary>
        ///Genera el codigo de los accidentes
        /// </summary>
        /// <returns>
        /// Retorna una dato de tipo string
        /// </returns>
        public  string GenerarCod()
        {
            string cod = "";

            string[] letras = { "Z","1","2","3","4","5","6","7","8","2","9","10",
            "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N"};
            Random al = new Random();
            for (int i = 0; i < 4; i++)
            {
                int le = al.Next(0, letras.Length - 1);
                cod += letras[le];
            }
            return cod;
        }


    }
}
