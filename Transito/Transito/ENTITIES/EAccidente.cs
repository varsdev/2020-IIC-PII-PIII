﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transito.ENTITIES
{
    public class EAccidente
    {
        public string codigo { set; get; }
        public string ced_usu { set; get; }
        public int num_parte { set; get; }
        public string ced_juzg { set; get; }
        public string ced_ofic { set; get; }
        public string lugar { set; get; }
        public int num_registro { get; set; }
        public string placa { set; get; }
        public string estado { set; get; }
        public DateTime fecha { get; set; }
        public TimeSpan hora { get; set; }
        public double multa { get; set; }

        public override string ToString()
        {
            return codigo + " " + ced_usu + " " + num_parte + " " + ced_juzg + " " + ced_ofic + " " +
                lugar + " " + num_registro + " " + placa + " " + estado + " " + fecha + " " + multa;
        }
    }
}
