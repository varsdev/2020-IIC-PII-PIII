﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transito.ENTITIES
{
    public class EPersona
    {
        public string cedula { get; set; }
        public string nombre { get; set; }
        public string contrasenna{ get; set; }
        public int tipo { get; set; }

        public override string ToString()
        {
            return cedula + " " + nombre + " " + contrasenna + " " + tipo; 
        }
    }
}
