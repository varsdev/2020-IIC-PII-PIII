﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transito.ENTITIES
{
    public class EVehiculo
    {
        public string placa { get; set; }
        public int anno { get; set; }
        public string marca { get; set; }
        public string color { get; set; }
        public int tipo { get; set; }
        public string cedPropietario { get; set; }

        //public override string ToString()
        //{
        //    return placa + " " + anno + " " + marca + " " + color + " " + tipo + " " + cedPropietario ;
        //}
        public override string ToString()
        {
            return placa;
        }
    }
}
